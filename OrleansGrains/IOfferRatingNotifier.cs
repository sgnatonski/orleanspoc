﻿using System.Collections.Generic;
using Orleans;

namespace OrleansGrains
{
    public interface IOfferRatingNotifier : IGrainObserver
    {
        void NotifyRatingsSorted(long productId, IEnumerable<long> sortedOfferIds);
    }
}