using System.Threading.Tasks;
using Orleans;

namespace OrleansTest
{
    public interface IOffer : IGrainWithIntegerKey
    {
        Task Activate(long sellerId, long productId);
        Task Deactivate();
        Task OfferChanged(double rating);
        Task SellerRatingChanged(double newRating);
    }
}