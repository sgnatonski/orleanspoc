﻿using System.Collections.Generic;

namespace OrleansTest
{
    public class ProductState
    {
        public Dictionary<long, double> Offers { get; set; } = new Dictionary<long, double>();
    }
}