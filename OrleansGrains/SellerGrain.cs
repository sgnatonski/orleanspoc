using System;
using System.Linq;
using System.Threading.Tasks;
using Orleans;
using OrleansTest;

namespace OrleansGrains
{
    public class SellerGrain : Grain<SellerState>, ISeller
    {
        private readonly OrleansStatePersistencyPolicy _policy = OrleansStatePersistencyPolicy.Every(TimeSpan.FromSeconds(10));

        public async Task OwnOffer(long offerId)
        {
            if (!State.Offers.Contains(offerId))
            {
                State.Offers.Add(offerId);
                await _policy.PersistIfNeeded(WriteStateAsync);
                //await GrainFactory.GetGrain<IOffer>(offerId).SellerRatingChanged(State.Rating);
            }
        }

        public async Task ReleaseOffer(long offerId)
        {
            State.Offers.Remove(offerId);
            await _policy.PersistIfNeeded(WriteStateAsync);
        }

        public async Task RatingChanged(double newRating)
        {
            State.Rating = newRating;
            foreach (var task in State.Offers.Select(x => this.GrainFactory.GetGrain<IOffer>(x)))
                await task.SellerRatingChanged(newRating);
        }
    }
}