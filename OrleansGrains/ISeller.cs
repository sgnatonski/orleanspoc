using System.Threading.Tasks;
using Orleans;

namespace OrleansTest
{
    public interface ISeller : IGrainWithIntegerKey
    {
        Task OwnOffer(long offerId);
        Task ReleaseOffer(long offerId);
        Task RatingChanged(double newRating);
    }
}