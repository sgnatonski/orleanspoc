﻿namespace OrleansTest
{
    public class OfferState
    {
        public long SellerId { get; set; }
        public long ProductId { get; set; }
        public double Rating { get; set; }
        public double SellerRating { get; set; }
    }
}