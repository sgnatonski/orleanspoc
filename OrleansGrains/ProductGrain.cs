using System;
using System.Linq;
using System.Threading.Tasks;
using Orleans;
using OrleansTest;

namespace OrleansGrains
{
    public class ProductGrain : Grain<ProductState>, IProduct
    {
        private readonly OrleansStatePersistencyPolicy _policy = OrleansStatePersistencyPolicy.Every(TimeSpan.FromSeconds(10));
        private ObserverSubscriptionManager<IOfferRatingNotifier> _subsManager;

        public override async Task OnActivateAsync()
        {
            _subsManager = new ObserverSubscriptionManager<IOfferRatingNotifier>();
            await base.OnActivateAsync();
        }

        public Task Subscribe(IOfferRatingNotifier observer)
        {
            _subsManager.Subscribe(observer);
            return TaskDone.Done;
        }
        
        public Task UnSubscribe(IOfferRatingNotifier observer)
        {
            _subsManager.Unsubscribe(observer);
            return TaskDone.Done;
        }

        public async Task AddOffer(long offerId, double rating)
        {
            State.Offers[offerId] = rating;
            RatingsChanged();
            await _policy.PersistIfNeeded(WriteStateAsync);
        }

        public async Task RemoveOffer(long offerId)
        {
            State.Offers.Remove(offerId);
            RatingsChanged();
            await _policy.PersistIfNeeded(WriteStateAsync);
        }

        public async Task OfferRatingChanged(long offerId, double rating)
        {
            State.Offers[offerId] = rating;
            RatingsChanged();
            await _policy.PersistIfNeeded(WriteStateAsync);
        }

        private void RatingsChanged()
        {
            _subsManager.Notify(s => s.NotifyRatingsSorted(this.GetPrimaryKeyLong(), State.Offers.OrderBy(x => x.Value).Select(x => x.Key).ToList()));
        }
    }
}