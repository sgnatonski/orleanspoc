﻿using System;
using System.Threading.Tasks;
using Orleans;
using OrleansTest;

namespace OrleansGrains
{
    public class OfferGrain : Grain<OfferState>, IOffer
    {
        private readonly OrleansStatePersistencyPolicy _policy = OrleansStatePersistencyPolicy.Every(TimeSpan.FromSeconds(10));

        private double Rating => State.Rating + State.SellerRating;

        public async Task Activate(long sellerId, long productId)
        {
            State.SellerId = sellerId;
            State.ProductId = productId;
            await _policy.PersistIfNeeded(WriteStateAsync);
            await GrainFactory.GetGrain<ISeller>(State.SellerId).OwnOffer(this.GetPrimaryKeyLong());
            await GrainFactory.GetGrain<IProduct>(State.ProductId).AddOffer(this.GetPrimaryKeyLong(), Rating);
        }

        public async Task Deactivate()
        {
            await GrainFactory.GetGrain<ISeller>(State.SellerId).ReleaseOffer(this.GetPrimaryKeyLong());
            await GrainFactory.GetGrain<IProduct>(State.ProductId).RemoveOffer(this.GetPrimaryKeyLong());
        }

        public async Task OfferChanged(double rating)
        {
            State.Rating = rating;
            await _policy.PersistIfNeeded(WriteStateAsync);
            await GrainFactory.GetGrain<IProduct>(State.ProductId).OfferRatingChanged(this.GetPrimaryKeyLong(), Rating);
        }

        public async Task SellerRatingChanged(double newRating)
        {
            State.SellerRating = newRating;
            await _policy.PersistIfNeeded(WriteStateAsync);
            await GrainFactory.GetGrain<IProduct>(State.ProductId).OfferRatingChanged(this.GetPrimaryKeyLong(), Rating);
        }
    }
}
