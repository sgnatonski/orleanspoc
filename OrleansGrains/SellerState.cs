﻿using System.Collections.Generic;

namespace OrleansTest
{
    public class SellerState
    {
        public IList<long> Offers { get; set; } = new List<long>();
        public double Rating { get; set; }
    }
}