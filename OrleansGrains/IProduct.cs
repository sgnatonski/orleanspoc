using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;
using OrleansGrains;

namespace OrleansTest
{
    public interface IProduct : IGrainWithIntegerKey
    {
        Task AddOffer(long offerId, double rating);
        Task RemoveOffer(long offerId);
        Task  OfferRatingChanged(long offerId, double rating);

        Task Subscribe(IOfferRatingNotifier observer);
        Task UnSubscribe(IOfferRatingNotifier observer);
    }
}