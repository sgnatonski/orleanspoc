﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orleans;
using Orleans.Runtime.Configuration;
using Orleans.Runtime.Host;
using OrleansGrains;

namespace OrleansTest
{
    class Program
    {
        private static SiloHost _siloHost;

        static void Main(string[] args)
        {
            var hostDomain = AppDomain.CreateDomain("OrleansHost", null, new AppDomainSetup
            {
                AppDomainInitializer = InitSilo,
                AppDomainInitializerArguments = args,
            });
            
            GrainClient.Initialize(ClientConfiguration.StandardLoad());

            Console.WriteLine("Orleans Silo is running.\nPress Enter to terminate...");

            //var c = new OfferRatingNotifier();
            //var obj = GrainClient.GrainFactory.CreateObjectReference<IOfferRatingNotifier>(c).Result;

            //GrainClient.GrainFactory.GetGrain<IProduct>(1).Subscribe(obj).Wait();
            //GrainClient.GrainFactory.GetGrain<IProduct>(2).Subscribe(obj).Wait();
            //GrainClient.GrainFactory.GetGrain<IProduct>(3).Subscribe(obj).Wait();

            GrainClient.GrainFactory.GetGrain<IOffer>(1).Activate(1, 1).Wait();
            GrainClient.GrainFactory.GetGrain<IOffer>(2).Activate(1, 2).Wait();
            GrainClient.GrainFactory.GetGrain<IOffer>(3).Activate(2, 1).Wait();
            GrainClient.GrainFactory.GetGrain<IOffer>(4).Activate(2, 3).Wait();
            GrainClient.GrainFactory.GetGrain<IOffer>(5).Activate(1, 3).Wait();

            GrainClient.GrainFactory.GetGrain<ISeller>(1).RatingChanged(1000).Wait();
            GrainClient.GrainFactory.GetGrain<ISeller>(2).RatingChanged(900).Wait();

            GrainClient.GrainFactory.GetGrain<IOffer>(3).OfferChanged(200).Wait();

            GrainClient.GrainFactory.GetGrain<ISeller>(1).RatingChanged(2000).Wait();
            GrainClient.GrainFactory.GetGrain<IOffer>(6).Activate(1, 1).Wait();
            GrainClient.GrainFactory.GetGrain<IOffer>(6).OfferChanged(100).Wait();

            GrainClient.GrainFactory.GetGrain<IOffer>(6).Deactivate().Wait();

            #region Wanna stress test? ;)
            /*
            var rnd = new Random();

            for (int i = 0; i < 10000; i++)
            {
                GrainClient.GrainFactory.GetGrain<IOffer>(rnd.Next(1, 1000))
                    .Activate(rnd.Next(1, 3), rnd.Next(1, 3))
                    .Wait();
            }

            for (int i = 0; i < 10000; i++)
            {
                GrainClient.GrainFactory.GetGrain<IOffer>(rnd.Next(1, 1000))
                    .OfferChanged(rnd.Next(1, 1000))
                    .Wait();
            }*/
            #endregion

            Console.ReadLine();

            hostDomain.DoCallBack(ShutdownSilo);
        }

        static void InitSilo(string[] args)
        {
            _siloHost = new SiloHost(System.Net.Dns.GetHostName()) {ConfigFileName = "OrleansConfiguration.xml"};
            _siloHost.InitializeOrleansSilo();
            
            var startedok = _siloHost.StartOrleansSilo();
            if (!startedok)
                throw new SystemException($"Failed to start Orleans silo '{_siloHost.Name}' as a {_siloHost.Type} node");

        }

        static void ShutdownSilo()
        {
            if (_siloHost != null)
            {
                _siloHost.Dispose();
                GC.SuppressFinalize(_siloHost);
                _siloHost = null;
            }
        }
    }
}
