﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansGrains
{
    public class OfferRatingNotifier : IOfferRatingNotifier
    {
        public void NotifyRatingsSorted(long productId, IEnumerable<long> sortedOfferIds)
        {
            Console.WriteLine($"{productId}\n{string.Join(", ", sortedOfferIds)}\n");
        }
    }
}
